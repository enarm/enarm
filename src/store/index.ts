import { createStore, combineReducers, applyMiddleware } from 'redux'
import { homeReducer } from '../domain/Categories/reducer';
import logger from 'redux-logger'

export const rootReducer = combineReducers({
  home: homeReducer,
});

export default createStore(
  rootReducer,
  applyMiddleware(logger),
);
export type RootState = ReturnType<typeof rootReducer>
