import { Category } from './types';
import { CATEGORIES_FETCH_SUCCESS, CATEGORIES_FETCH_FAILURE, TOGGLE_CATEGORY, TOGGLE_SUBCATEGORY, CategoriesAction } from './actionTypes';

class ActionCreators {
  /** Cambia las categorias y les agrega el campo selected para usar el switch */
  setCategories(categories: Category[]): CategoriesAction {
    return {
      type: CATEGORIES_FETCH_SUCCESS,
      categories,
    };
  }

  /** Muestra que pasó un error en dado caso que la api falle */
  showError(): CategoriesAction {
    return {
      type: CATEGORIES_FETCH_FAILURE,
    }
  }

  /** Hace switch a la categoría */
  toggleCategory(categoryIndex: number): CategoriesAction {
    return {
      type: TOGGLE_CATEGORY,
      categoryIndex,
    }
  }

  /** Hace switch a la subcategoría */
  toggleSubcategory(categoryIndex: number, subcategoryIndex: number): CategoriesAction {
    return {
      type: TOGGLE_SUBCATEGORY,
      categoryIndex,
      subcategoryIndex,
    }
  }
}

export default new ActionCreators();