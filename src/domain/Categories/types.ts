
export interface Subcategory {
  id_subcategoria: number;

  /** Nombre de la subcategoría */
  descripcion: string;

  /** Propiedad que uso para seleccionar la subcategoría */
  selected: boolean;
}

export interface Category {
  /** id de la subcategoría */
  id_categoria: number;

  /** Es la descripción de la categoría */
  descripcion: string;

  /** Arreglo de subcategorias */
  subcategorias: Subcategory[];

  /** Propiedad que uso para seleccionar la categoría */
  selected: boolean;
}

export interface CategoriesState {
  data: Category[];

  /** Esto lo voy a necesitar en algún momento */
  isLoading: boolean;

  isError: boolean;

};

export interface CategoriesListProps {
  categories: Category[];
}

export interface SubcategoriesListProps {
  subcategories: Subcategory[];
  categoryIndex: number;
}
