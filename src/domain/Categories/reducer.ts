import { CategoriesState } from './types';
import { CATEGORIES_FETCH_SUCCESS, CATEGORIES_FETCH_INIT, CATEGORIES_FETCH_FAILURE, TOGGLE_CATEGORY, TOGGLE_SUBCATEGORY, CategoriesAction, CategoriesFetchSuccessAction, CategoriesSelectAction, SubcategoriesSelectAction } from './actionTypes';
import { categorySchema } from './schema/categories.schema';
import { normalize } from 'normalizr';

export const homeInitialData = {
  data: [], isLoading: false, isError: false,
};

export const homeReducer = (state: CategoriesState = homeInitialData, action: CategoriesAction) => {
  switch (action.type) {
    case CATEGORIES_FETCH_INIT:
      return {
        ...state, isLoading: true, isError: false,
      };

    case CATEGORIES_FETCH_SUCCESS:
      return applySetCategories(state, action);

    case CATEGORIES_FETCH_FAILURE:
      return {
        ...state, isLoading: false, isError: true,
      };

    case TOGGLE_CATEGORY:
      return applyCategoryToggle(state, action);

    case TOGGLE_SUBCATEGORY:
      return applySubcategoryToggle(state, action);
    default:
      return state;
  }
};

function applySetCategories(state: CategoriesState, action: CategoriesFetchSuccessAction) {
  let categories = action.categories.map(
    (category) => ({ ...category, selected: false })
  );
  //console.log('que hay?: ', action.categories)
  // const normalizedCategories = normalize(action.categories, [categorySchema]);

  return {
    ...state,
    data: categories,
  };
}

function applyCategoryToggle(state: CategoriesState, action: CategoriesSelectAction) {
  const categories = state.data;
  const categoryIndex = action.categoryIndex;
  const categorySelected = categories[categoryIndex];
  const toggleCategory = !categorySelected.selected;
  const newCategory = { ...categorySelected, selected: toggleCategory };
  categories[categoryIndex] = newCategory;
  categories[categoryIndex].subcategorias = categories[categoryIndex].subcategorias.map(
    (subcategory) => ({ ...subcategory, selected: toggleCategory })
  );

  return {
    ...state,
    data: categories
  };
}

function applySubcategoryToggle(
  state: CategoriesState,
  { categoryIndex, subcategoryIndex }: SubcategoriesSelectAction
) {
  const categories = state.data;
  const category = categories[categoryIndex]
  const subcategory = category.subcategorias[subcategoryIndex];
  category.subcategorias[subcategoryIndex] = { ...subcategory, selected: !subcategory.selected }

  categories[categoryIndex] = category;

  return {
    ...state,
    date: categories,
  }
}
