
class Api {
  async fetchCategorias() {
    const categoriesResponse = await fetch('http://localhost:8000/api/categorias-y-subcategorias');
    return categoriesResponse.json();
  }
}

export default new Api();