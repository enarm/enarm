import { schema } from 'normalizr';

// Define a users schema
const subcategorySchema = new schema.Entity('subcategoria', {}, { idAttribute: 'id_subcategoria' });

// Define your article
export const categorySchema = new schema.Entity('categorias',
  {
    subcategorias: [subcategorySchema]
  },
  {
    idAttribute: 'id_categoria',
  }
);


