import { useEffect, useState } from 'react';

export const useFetch = (url: string, deps: any[] = [], options: any = {}) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const fetchUrlPrefix = 'http://localhost:8000/api/';

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const response = await fetch(`${fetchUrlPrefix}${url}`, options);
        const json = await response.json();
        setResponse(json);
        setIsLoading(false);
      } catch (error) {
        setError(error.message)
      }
    };
    fetchData();
  }, deps);

  return { response, isLoading, error }
}