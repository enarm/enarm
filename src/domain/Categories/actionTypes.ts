import { Category } from './types';

export const CATEGORIES_FETCH_SUCCESS = 'CATEGORIES_FETCH_SUCCESS';
export const CATEGORIES_FETCH_INIT = 'CATEGORIES_FETCH_INIT';
export const CATEGORIES_FETCH_FAILURE = 'CATEGORIES_FETCH_FAILURE';
export const TOGGLE_CATEGORY = 'TOGGLE_CATEGORY';
export const TOGGLE_SUBCATEGORY = 'TOGGLE_SUBCATEGORY';

interface CategoriesFetchInitAction {
  type: typeof CATEGORIES_FETCH_INIT;
}

export interface CategoriesFetchSuccessAction {
  type: typeof CATEGORIES_FETCH_SUCCESS;

  /** Categorías que vienen de la api */
  categories: Category[];
}

interface CategoriesFetchFailureAction {
  type: typeof CATEGORIES_FETCH_FAILURE;
}

export interface CategoriesSelectAction {
  type: typeof TOGGLE_CATEGORY;

  /** Index de la categoría en el arreglo */
  categoryIndex: number;
}

export interface SubcategoriesSelectAction {
  type: typeof TOGGLE_SUBCATEGORY;

  /** Index de la categoría en el arreglo */
  categoryIndex: number;

  /** Index de la subcategoría en el arreglo */
  subcategoryIndex: number;
}

export type CategoriesAction =
  | CategoriesFetchInitAction
  | CategoriesFetchSuccessAction
  | CategoriesFetchFailureAction
  | CategoriesSelectAction
  | SubcategoriesSelectAction 