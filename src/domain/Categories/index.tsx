import React, { Fragment, useEffect } from 'react';
import ContainerApp from '../../components/ContainerApp';
import { Text, List, ListItem, Left, Switch, Right, Button } from "native-base";
import api from './api';
import actionCreators from './actionCreators';
import { RootState } from '../../store';
import { getCategoriesState } from './selectors';
import { useDispatch, useSelector } from 'react-redux';
import { CategoriesListProps, SubcategoriesListProps } from './types';
import { useNavigation } from '@react-navigation/native';

export default function Categories() {
  const categories = useSelector((state: RootState) => getCategoriesState(state));
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const fetchCategoriesAndSubcategories = async () => {
    try {
      const categoriesResponse = await api.fetchCategorias();
      dispatch(actionCreators.setCategories(categoriesResponse));
    } catch (err) {
      dispatch(actionCreators.showError())
    }
  }

  useEffect(() => {
    fetchCategoriesAndSubcategories();
  }, [])

  if (categories.isError) {
    return <Text> Algo salió mal :(</Text>
  }

  return (
    <ContainerApp>
      <CategoriesList categories={categories.data} />
      <Button primary>
        <Text onPress={() => navigation.navigate('questions', {})}>Go to questions </Text>
      </Button>
    </ContainerApp>
  );
}

const CategoriesList = ({ categories }: CategoriesListProps) => {
  const dispatch = useDispatch();
  const toggleCategorySelected = (categoryIndex: number) => {
    return () => {
      dispatch(actionCreators.toggleCategory(categoryIndex));
    };
  }

  return (
    <List>
      {
        categories.map((category, categoryIndex: number) => (
          <Fragment key={category.id_categoria}>
            <ListItem
              itemDivider
              selected={category.selected}
              onPress={toggleCategorySelected(categoryIndex)}
            >
              <Left>
                <Text>{category.descripcion}</Text>
              </Left>
              <Right>
                <Switch onValueChange={toggleCategorySelected(categoryIndex)} value={category.selected} />
              </Right>
            </ListItem>
            <SubcategoriesList subcategories={category.subcategorias} categoryIndex={categoryIndex} />
          </Fragment>
        ))
      }
    </List>
  )
}

const SubcategoriesList = ({ subcategories, categoryIndex }: SubcategoriesListProps) => {
  const dispatch = useDispatch();
  const toggleSubcategorySelected = (categoryIndex: number, subCategoryIndex: number) => {
    return () => {
      dispatch(actionCreators.toggleSubcategory(categoryIndex, subCategoryIndex));
    };
  }

  return (
    <Fragment>
      {
        subcategories.map((subcategory, subcategoryIndex: number) => (
          <ListItem
            key={subcategory.id_subcategoria}
            onPress={toggleSubcategorySelected(categoryIndex, subcategoryIndex)}
            selected={subcategory.selected}
          >
            <Left>
              <Text>{subcategory.descripcion}</Text>
            </Left>
            <Right>
              <Switch value={subcategory.selected} onValueChange={toggleSubcategorySelected(categoryIndex, subcategoryIndex)} />
            </Right>
          </ListItem>
        ))
      }
    </Fragment>
  )
}
