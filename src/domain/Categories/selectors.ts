import { RootState } from '../../store';
import { CategoriesState } from './types';

/**
 * Regresa el estado de las categorias.
 */
export const getCategoriesState = (state: RootState): CategoriesState => {
  return state.home;
}