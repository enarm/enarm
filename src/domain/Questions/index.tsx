import React, { useState } from 'react';
import { Button, Col, Content, Footer, H3, Icon, Left, ListItem, Radio, Right, Text, View } from 'native-base';
import styles from './style';
import { Grid, Row } from 'react-native-easy-grid';
import { QuestionHeaderProps } from './types';

const PreguntaPrueba = "Paciente varón de 65 años de edad que acude a nuestra consulta por presentar desde hace varios meses clínica miccional de polaquiuria y nicturia.En sus antecedentes personales no existen hábitos tóxicos ni alergias a fármacos, desde hace aproximadamente cinco años está a tratamiento con Nifedipino por Hipertensión arterial y con Simvastatina por dislipemia.";

export default function Preguntas(params: any) {
  return (
    <>
      <Grid>
        <Row size={50}>
          <HeaderQuestion text={PreguntaPrueba} />
        </Row>
        <Row size={50}>
          <Responses />
        </Row>
      </Grid>
      <Footer>
        <Grid style={{ alignItems: 'flex-end' }}>
          <Row>
            <Button transparent iconRight>
              <Text>Siguiente</Text>
              <Icon name='arrow-forward' />
            </Button>
          </Row>
        </Grid>
      </Footer>
    </>
  )
}

const HeaderQuestion = ({ text }: QuestionHeaderProps) => {
  return (
    <View style={styles.headerQuestion}>
      <H3 style={styles.questionTitle}>
        Pregunta 1 de 120
      </H3>
      <Text style={styles.questionText}>
        {text}
      </Text>
    </View>
  )
}

const Responses = () => {
  const [radioIndex, setRadioIndex] = useState(-1);

  return (
    <Content>
      <ListItem onPress={() => setRadioIndex(0)}>
        <Left>
          <Text>
            ¿En este paciente con esta situación clínica cual sería la hipótesis diagnóstica a valorar?
          </Text>
        </Left>
        <Right>
          <Radio selected={radioIndex === 0} />
        </Right>
      </ListItem>
      <ListItem onPress={() => setRadioIndex(1)}>
        <Left>
          <Text>
            ¿En este paciente con esta situación clínica cual sería la hipótesis diagnóstica a valorar?
          </Text>
        </Left>
        <Right>
          <Radio selected={radioIndex === 1} />
        </Right>
      </ListItem>
      <ListItem onPress={() => setRadioIndex(2)}>
        <Left>
          <Text>
            ¿En este paciente con esta situación clínica cual sería la hipótesis diagnóstica a valorar?
          </Text>
        </Left>
        <Right>
          <Radio selected={radioIndex === 2} />
        </Right>
      </ListItem>
    </Content>
  )
}
