import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import themeColors from '../../../native-base-theme/variables/commonColor';

export default StyleSheet.create({
  headerQuestion: {
    paddingTop: getStatusBarHeight(),
    paddingHorizontal: 10,
    borderBottomRightRadius: 27,
    borderBottomLeftRadius: 27,
    backgroundColor: themeColors.brandPrimary,
  },
  questionTitle: {
    color: 'white',
    textAlign: 'center',
    marginBottom: 10,
  },
  questionText: {
    color: 'white',
    textAlign: 'justify',
  },

  color: {
    backgroundColor: 'red'
  }
});
