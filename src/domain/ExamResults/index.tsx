import React from 'react';
import ContainerApp from '../../components/ContainerApp';
import { Container, Header, Content, Accordion, Button, Text } from "native-base";

const dataArray = [
  {
    title: "First Element", content: "jajarajajay"
  },
  { title: "Second Element", content: "Lorem ipsum dolor sit amet" },
  { title: "Third Element", content: "Lorem ipsum dolor sit amet" }
];
function renderContent(item: any) {
  return (
    <Button light><Text> {item.content} </Text></Button>
  );
}
export default function Home(props) {
  return (
    <ContainerApp>
      <Container>
        <Header />
        <Content padder>
          <Accordion
            dataArray={dataArray}
            headerStyle={{ backgroundColor: "#b7daf8" }}
            contentStyle={{ backgroundColor: "#ddecf8" }}
            renderContent={renderContent}
          />
        </Content>
      </Container>
    </ContainerApp>
  );
}