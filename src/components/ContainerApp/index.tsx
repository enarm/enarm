import React, { useState, useEffect } from "react";
import {
  Container as NativeBaseContainer,
  Header,
} from "native-base";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { ContainerProps } from './types';

export default function ContainerApp({ children }: ContainerProps) {
  const [ready, setReady] = useState(false);

  useEffect(() => {
    Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      ...Ionicons.font
    }).then(() => setReady(true));
  }, []);

  if (ready) {
    return (
      <NativeBaseContainer>
        {children}
      </NativeBaseContainer>
    );
  } else {
    return <AppLoading />;
  }
}

const HeaderNativeBaseContainer = () => (
  <Header
    noLeft
    style={{
      paddingTop: getStatusBarHeight(),
      height: 54 + getStatusBarHeight()
    }}>
  </Header>
)