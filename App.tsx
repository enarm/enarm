import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Categories from './src/domain/Categories';
import { Provider } from 'react-redux';
import store from './src/store';
import Questions from './src/domain/Questions';

const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='categories'>
          <Stack.Screen name='categories' component={Categories} />
          <Stack.Screen name='questions' component={Questions} options={{
            headerShown: false,
            gestureEnabled: false
          }} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
